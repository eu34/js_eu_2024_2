var buttons = document.getElementsByTagName("button")
var inputs = document.getElementsByTagName("input")
var main = document.getElementsByTagName("main")
var colors = ["red", "blue", "green"]

buttons[0].addEventListener("click", () => {
  var div_count = inputs[0].value
  console.log(div_count)
  main[0].innerText = "";
  for (let i = 0; i < parseInt(div_count); i++) {
    add_div().addEventListener("click", function () {
      switch (this.style.backgroundColor) {
        case "red": this.parentElement.removeChild(this)
          break
        case "green": {
          add_div()
          add_div()
        }
          break
      }
    })
  }
})

function add_div() {
  var r_bg = Math.floor(Math.random() * colors.length)
  var div = document.createElement("div")
  div.style.backgroundColor = colors[r_bg]
  div.classList.add("div")
  main[0].appendChild(div)
  return div
}