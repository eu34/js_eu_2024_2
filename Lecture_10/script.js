function add_ball(){
  var bal = document.createElement("div")
  bal.classList.add("ball")
  var colors = {  
                  'r': Math.floor(Math.random()*256), 
                  'g':Math.floor(Math.random()*256), 
                  'b':Math.floor(Math.random()*256) 
                }
   var text_colors = {  
                  'r': Math.floor(Math.random()*256), 
                  'g':Math.floor(Math.random()*256), 
                  'b':Math.floor(Math.random()*256) 
                }              
  bal.style.background = `rgb(${colors.r}, ${colors.g}, ${colors.b})`
  bal.style.color = `rgb(${text_colors.r}, ${text_colors.g}, ${text_colors.b})`
  bal.innerHTML = Math.floor(Math.random()*90)+10
  var main = document.getElementsByTagName("main")
  var sq =  main[0]
  sq.appendChild(bal)
}

function start(){
  st = setInterval(add_ball, 500)
}

function stop(){
  clearInterval(st);
}