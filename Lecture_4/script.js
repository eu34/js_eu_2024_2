function get_number_of_even(num){  
    n_even = 0;
    while(num!=0){
      d = num % 10
      num = Math.floor(num / 10)
      if(d%2==0){
        n_even++
      }
    }
    return n_even;
}

function generate_random_number(n, m, _N){
      for(i=0; i<_N; i++){
        r =  Math.floor(Math.random() * (m-n)) + n
        document.write(`<div>${r} - ${get_number_of_even(r)}</div>`)
      }
}

// generate_random_number(4000, 6000, 10)

function print_len_of_str(s){
  h_symbol = 0
  for(i=0; i<s.length; i++){
      if(s[i].toUpperCase()=='H'){
        h_symbol++
      }
  }
  sp_symbol = 0
  for(i=0; i<s.length; i++){
      if(s[i]==' '){
        sp_symbol++
      }
  }
  document.write(`<div>String - ${s}</div>`)
  document.write(`<div>Length of String - ${s.length}</div>`)
  document.write(`<div>Length of String without spaces - ${s.length - sp_symbol}</div>`)
  document.write(`<div>Number of H Symbol - ${h_symbol}</div>`)
}

// print_len_of_str("Hello JS Hi Html hI  ")

function generate_random_s(N){
  a = "abcdefghijklmnopqr"
  w = ""
  for(i=0; i<N; i++){
    w += a[Math.floor(Math.random()*a.length)]
  }
  document.write(`<div>Random String - ${w}</div>`)
}


generate_random_s(50)
