function f1(){
  document.write("European University<br>")
}

// f1()

function f2(){
  // for(i=0; i<10; i++){
  //   document.write("European University<br>")
  // }
  i = 0
  while(i < 10){
    document.write("European University<br>")
    i++
  }
}

// f2()
// for(i=0; i<10; i++){
//   f1();
// }

m = [23, 2.7,  "Eu", "Student", 2024, false]
// document.write(m+"<hr>")
// document.write(m[3]+"<hr>")

function f3(arr){
  for(i=0; i<arr.length; i++){
    document.write(`<p>${i} - ${arr[i]}</p>`)
  }
}

// f3(m)

function f4(N){
  m = []
  i = 0
  a = 2
  while(i != N){
    m.push(a)  
    i++
    a += 10
  }
  return m
}

m2 = f4(5)
// 2, 12, 22, 32, 42, 52,  . . . 17 ცალს რიცხვს

f3(m2)
